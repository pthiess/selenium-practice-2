package selenium.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void openPage() {
        driver.get("https://www.saucedemo.com/");
    }


    public WebElement getLoginButton() {
        return driver.findElement(By.cssSelector("#login-button"));
    }

    public WebElement getLoginFailureMessage() {
        return driver.findElement(By.cssSelector(".error-message-container"));
    }


}
