package selenium.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HerokuUploadPage {

    private final WebDriver webDriver;

    public HerokuUploadPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void openPage() {
        webDriver.get("https://the-internet.herokuapp.com/upload");
    }

    public WebElement fileUpload() {
        return webDriver.findElement(By.cssSelector("#file-upload"));
    }

    public WebElement submitUploadButton() {
        return webDriver.findElement(By.cssSelector("#file-submit"));
    }

    public WebElement uploadSuccessText() {
        return webDriver.findElement(By.cssSelector("#uploaded-files"));
    }
}
