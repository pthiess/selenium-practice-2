package selenium.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BootstrapChecksRadioPage {

    private final WebDriver webDriver;

    public BootstrapChecksRadioPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void openPage() {
        webDriver.get("https://getbootstrap.com/docs/5.3/forms/checks-radios/");
    }

    public WebElement checkedCheckbox() {
        return webDriver.findElement(By.cssSelector("#flexCheckChecked"));
    }

    public WebElement defaultCheckbox() {
        return webDriver.findElement(By.cssSelector("#flexCheckDefault"));
    }
}
