package selenium.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BootstrapSelectPage {

    private final WebDriver webDriver;

    public BootstrapSelectPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void openPage() {
        webDriver.get("https://getbootstrap.com/docs/5.3/forms/select/");
    }

    public Select getDefaultSelect() {
        WebElement defaultSelection = webDriver.findElement(By.cssSelector("select[aria-label=\"Default select example\"]"));
        return new Select(defaultSelection);
    }

}
