package selenium.step_defs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.nio.file.Path;

import static selenium.setup.Setup.*;

public class InputExperimentsStepDefs {


    @When("I select the checkbox {string}")
    public void whenISelectCheckbox(String checkboxName) {
        WebElement checkbox = switch (checkboxName) {
            case "Checked checkbox" -> bootstrapChecksRadioPage.checkedCheckbox();
            case "Default checkbox" -> bootstrapChecksRadioPage.defaultCheckbox();
            default -> throw new IllegalArgumentException("Unexpected value: " + checkboxName);
        };

        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", checkbox);
        checkbox.click();
    }


    @When("I select {string} in the default selection")
    public void selectInDefaultSection(String selectionValue) {
        bootstrapSelectPage.getDefaultSelect().selectByVisibleText(selectionValue);
    }

    @When("I choose the file {string} to upload on heroku")
    public void chooseFileToUploadOnHeroku(String fileName) {
        String relativePath = switch (fileName) {
            case "testfile1.txt" -> "src/test/resources/testfile1.txt";
            default -> throw new IllegalArgumentException("Unexpected value: " + fileName);
        };

        Path filePath = Path.of(relativePath);
        herokuUploadPage.fileUpload().sendKeys(filePath.toAbsolutePath().toString());
    }

    @When("I click the heroku upload button")
    public void iClickUploadButton() {
        herokuUploadPage.submitUploadButton().click();
    }

    @Then("The selection checkbox {string} is checked")
    public void checkBoxIsChecked(String checkboxName) {
        WebElement checkbox = switch (checkboxName) {
            case "Default checkbox" -> bootstrapChecksRadioPage.checkedCheckbox();
            default -> throw new IllegalArgumentException("Unexpected value: " + checkboxName);
        };

        Assertions.assertTrue(checkbox.isSelected());
    }

    @Then("The current selection option is {string}")
    public void selectionHasCorrectOption(String expectedOption) {
        String selectedOption = bootstrapSelectPage.getDefaultSelect().getFirstSelectedOption().getText();
        Assertions.assertEquals(expectedOption, selectedOption);
    }

    @Then("The upload success text has the file-name {string}")
    public void uploadSuccessIncludesText(String expectedFileName) {
        String actualFileName = herokuUploadPage.uploadSuccessText().getText();
        Assertions.assertEquals(expectedFileName, actualFileName);
    }


}
