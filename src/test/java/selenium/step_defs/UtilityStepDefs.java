package selenium.step_defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import static selenium.setup.Setup.*;

public class UtilityStepDefs {

    @Given("I start the chrome browser")
    public void startTheBrowser() {
        setupDriverAndPages();
    }

    @When("I open the {string} page")
    public void openBootstrapPage(String pageName) {
        switch (pageName) {
            case "bootstrap forms select" -> bootstrapSelectPage.openPage();
            case "bootstrap checks radio" -> bootstrapChecksRadioPage.openPage();
            case "heroku upload" -> herokuUploadPage.openPage();
            default -> throw new IllegalArgumentException("Unexpected value: " + pageName);
        }
    }


}
