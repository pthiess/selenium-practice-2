package selenium.step_defs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;

import static selenium.setup.Setup.*;

public class LoginPageStepDefs {

    @When("I load the login page")
    public void loadLoginPage() {
        loginPage.openPage();
    }


    @When("The login button can be pressed")
    public void canPressLoginButton() {
        loginPage.openPage();
        WebElement loginButton = loginPage.getLoginButton();
        loginButton.click();
    }

    @Then("The {string} login failure message is displayed")
    public void loginButtonHasCorrectText(String failureMessage) {
        WebElement loginFailureMessage = loginPage.getLoginFailureMessage();

        Assertions.assertEquals(failureMessage, loginFailureMessage.getText());
    }
}
