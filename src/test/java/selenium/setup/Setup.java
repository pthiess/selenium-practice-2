package selenium.setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import selenium.page_objects.BootstrapChecksRadioPage;
import selenium.page_objects.BootstrapSelectPage;
import selenium.page_objects.HerokuUploadPage;
import selenium.page_objects.LoginPage;

public class Setup {

    public static WebDriver webDriver;
    public static LoginPage loginPage;
    public static BootstrapChecksRadioPage bootstrapChecksRadioPage;
    public static BootstrapSelectPage bootstrapSelectPage;
    public static HerokuUploadPage herokuUploadPage;

    public static void setupDriverAndPages() {
        ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1920,1080");

        webDriver = new ChromeDriver(chromeOptions);

        loginPage = new LoginPage(webDriver);
        bootstrapChecksRadioPage = new BootstrapChecksRadioPage(webDriver);
        bootstrapSelectPage = new BootstrapSelectPage(webDriver);
        herokuUploadPage = new HerokuUploadPage(webDriver);
    }
}
